## Description

A store api using express and ts.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

# production
$ npm start
```
