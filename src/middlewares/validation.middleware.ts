import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { NextFunction, Request, RequestHandler, Response } from 'express';
import ApiError from '../utils/ApiError';

export default function validationMiddleware<T>(
	type: any,
	skipMissingProperties = false,
): RequestHandler {
	return (req: Request, res: Response, next: NextFunction) => {
		validate(plainToClass(type, req.body), { skipMissingProperties }).then(
			(errors: ValidationError[]) => {
				if (errors.length > 0) {
					const message = errors
						.map((error: ValidationError) =>
							Object.values(error.constraints),
						)
						.join(', ');
					next(new ApiError(400, message));
				} else {
					next();
				}
			},
		);
	};
}
