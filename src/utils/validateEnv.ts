import * as Joi from 'joi';

export default function envValidate() {
	const envSchema = Joi.object().keys({
		PORT: Joi.number(),
		MONGO_HOST: Joi.string(),
		MONGO_PORT: Joi.number(),
		MONGO_DB: Joi.string(),
	});

	envSchema.validate(process.env);
}
