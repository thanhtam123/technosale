import * as express from 'express';
import ControllerInterface from './interfaces/controller.interface';
import { errorConverter, errorHandler } from './middlewares/error.middleware';
import * as cors from 'cors';
import catchAsync from './utils/catchAsync';

export default class App {
	public app: express.Application;
	public port: number;

	constructor(controllers: ControllerInterface[], port?: number) {
		this.app = express();
		this.port = port || Number(process.env.PORT) || 8000;

		// this.connectToTheDatabase();
		this.initializeMiddlewares();
		this.initializeControllers(controllers);

		this.app.get(
			'/api',
			catchAsync(
				async (
					req: express.Request,
					res: express.Response,
					next: express.NextFunction,
				) => {
					const message = 'hello';
					return { message };
				},
			),
		);

		this.initializeErrorHandler();
	}

	private initializeMiddlewares() {
		this.app.use(cors());

		// parse application/x-www-form-urlencoded
		this.app.use(express.urlencoded({ extended: true }));

		// parse application/json
		this.app.use(express.json());

		// log request to console
		this.app.use(this.loggerMiddleware);
	}

	private initializeControllers(controllers: ControllerInterface[]) {
		controllers.forEach((controller: ControllerInterface) => {
			this.app.use(controller.path, controller.router);
		});
	}

	private initializeErrorHandler() {
		this.app.use(errorConverter);
		this.app.use(errorHandler);
	}

	// log request to console
	private loggerMiddleware(
		req: express.Request,
		res: express.Response,
		next: express.NextFunction,
	) {
		console.log(
			`REQUEST: ${req.hostname} ${req.method} ${
				req.path
			} \n ${JSON.stringify(req.body)} \n `,
		);
		next();
	}

	public listen() {
		this.app.listen(this.port, () => {
			let server = console.log(`App listening on the port ${this.port}`);

			// handle process 'uncaughtException' and 'unhandledRejection' error
		});
	}

	// private connectToTheDatabase() {
	// 	const { MONGO_HOST, MONGO_PORT, MONGO_DB } = process.env;
	// 	const mongo_uri = `mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`;
	// 	mongoose
	// 		.connect(mongo_uri, {
	// 			useNewUrlParser: true, // error in @types/mongoose package
	// 			useUnifiedTopology: true,
	// 			useCreateIndex: true,
	// 			useFindAndModify: false,
	// 		})
	// 		.then(() => {
	// 			console.log('Successfully connect to MongoDB.');
	// 		})
	// 		.catch((err: Error) => {
	// 			console.error('MongoDB connection error', err);
	// 			process.exit();
	// 		});
	// }
}
